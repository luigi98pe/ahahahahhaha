package com.example.tecwebvolley;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tecwebvolley.volley.VolleySingleton;

public class VolleySingletonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley_singleton);

        final TextView textView = findViewById(R.id.text);

        RequestQueue queue = VolleySingleton.getInstance
                (this.getApplicationContext()).getRequestQueue();
        String url ="http://www.simonedinardodimaio.it/TecWEB/films.json";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        textView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("That didn't work!");
                Log.e("VOLLEY", error.getMessage());
            }
        });

        stringRequest.setTag("TAGFILMS");
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
    @Override
    protected void onStop () {
        super.onStop();
        if (VolleySingleton.getInstance(this).getRequestQueue() != null) {
            VolleySingleton.getInstance(this).getRequestQueue().cancelAll("TAGFILMS");
        }
    }

}
