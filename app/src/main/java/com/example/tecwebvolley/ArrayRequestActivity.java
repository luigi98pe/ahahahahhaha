package com.example.tecwebvolley;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.tecwebvolley.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.xml.transform.ErrorListener;

public class ArrayRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_array_request);

        final TextView textView = findViewById(R.id.text);

        RequestQueue queue = VolleySingleton.getInstance
                (this.getApplicationContext()).getRequestQueue();
        String url = "http://www.simonedinardodimaio.it/TecWEB/films.json";

// Request a string response from the provided URL.
        JsonArrayRequest jsonArrayRequest;
        jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject film = (JSONObject) response.get(i);
                        String titolo = film.getString("titolo");
                        textView.append(titolo + "\n");
                    }
                } catch (JSONException e) {
                    Log.e("JSONException", e.getMessage());
                    textView.setText("Qualcosa è andato storto");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("That didn't work!");
                Log.e("VOLLEY", error.getMessage());
            }


        });
        jsonArrayRequest.setTag("TAGMOVIE");
        VolleySingleton.getInstance(this).addToRequestQueue(jsonArrayRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (VolleySingleton.getInstance(this).getRequestQueue() != null) {
            VolleySingleton.getInstance(this).getRequestQueue().cancelAll("TAGMOVIE");
        }
    }

}
